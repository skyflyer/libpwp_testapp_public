package si.mobecgblelibrarytest;

import android.util.Log;

import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;


public class AndroidLoggingHandler extends java.util.logging.Handler {

    /**
     * A function to initialize the logging transfer between java.util.logging and Android.Log
     */
    public static void init() {
        Logger rootLogger = LogManager.getLogManager().getLogger("");
        /*
        // remove all existing handlers
        Handler[] handlers = rootLogger.getHandlers();
        for (Handler handler : handlers) {
            rootLogger.removeHandler(handler);
            Log.v("GadgetDBG", "removing a handler");
        }
        */

        // add this logger
        rootLogger.addHandler(new AndroidLoggingHandler());
    }

    @Override
    public void close() {}

    @Override
    public void flush() {}

    @Override
    public void publish(LogRecord record) {
        // check log level and other filters
        if (!super.isLoggable(record))
            return;

        // grab name (truncate it down to the allowed size limit) and tag
        String name = record.getLoggerName();
        final int maxLength = 30;
        String tag = name.length() > maxLength ? name.substring(name.length() - maxLength) : name;

        // call Android.Log with the gathered data
        try {
            int level = getAndroidLevel(record.getLevel());
            if (record.getThrown() == null) {
                Log.println(level, tag, record.getMessage());
            } else {
                Log.println(level, tag, Log.getStackTraceString(record.getThrown()));
            }
        } catch (RuntimeException e) {
            Log.e("AndroidLoggingHandler", "Error logging message", e);
        }
    }

    /**
     * Converts a {@link Logger} logging level into an Android logging level.
     *
     * @param level The {@link Logger} logging level.
     * @return The resulting Android logging level.
     */
    static int getAndroidLevel(Level level) {
        int value = level.intValue();
        if (value >= Level.SEVERE.intValue()) {
            return Log.ERROR;
        } else if (value >= Level.WARNING.intValue()) {
            return Log.WARN;
        } else if (value >= Level.INFO.intValue()) {
            return Log.INFO;
        } else if (value >= Level.FINER.intValue()) {
            return Log.DEBUG;
        } else
            return Log.VERBOSE;
    }
}
