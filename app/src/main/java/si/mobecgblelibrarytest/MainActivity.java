package si.mobecgblelibrarytest;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import si.ijs.e6.blelib.BleCommunicator;
import si.ijs.e6.blelib.BleScanInterface;
import si.ijs.e6.pcard.UnpackedEcgData;
import si.ijs.e6.pcard.*;
import si.ijs.e6.pcard.pwp.*;

/**
 * This is a minimal working example connecting to PCARD-like devices using 'Jožef Stefan Institute - E6' library and it's functionality
 *
 * Two modules need to be included as a dependency on this project
 *  - libPcard                  some basic pcard definitions
 *  - libBleAndroidWrapper      thread-safe wrapper around Android's BLE API
 *  - libPwp                    implements PCARD Wireless Protocol for communication with the gadget
 *  - libPcardTimeAlign         time alignment of received data (required for interpretation of PCARD gadgets' data)
 *
 * Code shows how to use the listed modules to connect to a device and receive data from it.
 * Data is stored into a file ecg.tt inside the devices' Documents directory
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, AdapterView.OnItemSelectedListener {

    GadgetThread thread;    // gadget connection thread

    //region class variables
    private static Logger logger = Logger.getLogger(GadgetThread.class.getName());
    final long ECG_OUTPUT_REPORT_TIME = 5000;

    Handler handler;        // handle UI changes

    //region user interface elements
    TextView macAddressTV;
    TextView pinTV;
    TextView frequency;
    Button start;
    Button end;
    SeekBar adcSelect;
    SeekBar modeSelect;
    SeekBar txPowerSelect;

    LinkedList<String> macSpinnerItems; // will hold scanned MAC address
    Button scanButton;
    Spinner macSpinner;

    ScrollView debugScroll;
    TextView debugOutput;
    Button clearDebug;
    Button helpButton;
    //endregion user interface elements

    boolean disconnectWhileSampling = false;
    boolean unsureCounter = false;
    // the last package counter
    long lastPackageCounter = -1;
    // counter of samples, that were not reported yet (number of received samples is reported in intervals)
    long numSamplesReceived = 0;
    // number of missed samples for the last report interval
    long samplesMissing = 0;
    // absolute sample counter
    long numSamplesReceivedFromStart = 0;
    // last absolute time when a packet was received
    long lastStreamingDataReportTime = 0;
    // firmware revision that is read upon connecting to a gadget (null until then)
    FirmwareRevision firmwareRevision;

    /**
     * Utility class for storing packets while time alignment is being performed
     */
    class TsPair implements TimestampPair {
        UnpackedEcgData ecgData;


        public TsPair(TsPair other) {
            ecgData = other.ecgData;
        }

        public TsPair(UnpackedEcgData e) {
            ecgData = e;
            // use ecgData.sampleCounter as storage for the raw counter (initially) and modified
            // values that will be stored through call to setCounter setter.
            ecgData.sampleCounter = ecgData.rawSampleCounter;
        }

        @Override
        public long getTimestamp() {
            return ecgData.times[0];
        }

        @Override
        public long getCounter() {
            return ecgData.sampleCounter;
        }

        @Override
        public void setTimestamp(long value) {
            ecgData.times[0] = value;
        }

        @Override
        public void setCounter(long value) {
            ecgData.sampleCounter = value;
        }
    }

    // instance that performs time alignment of incoming data packets
    PcardTimeAlignment<TsPair> timeAlign = new PcardTimeAlignment<>();
    // linear transformation function is used to transform raw samples to milliVolts
    LinearTransformationFunction ltf = null;

    //endregion class variables

    /**
     * Set this to true to write ECG data in raw form (not transformed to mV)
     */
    boolean writeEcgRawData = false;

    OutputStreamWriter ecgOutput;
    void storeEcgData() {
        Log.d("STORE", "Ecg data was stored ("+timeAlign.getOutputData().size()+")");

        if (ecgOutput != null) {
            try {
                for (TsPair data : timeAlign.getOutputData())
                    for (int value : data.ecgData.values)
                        if (writeEcgRawData)
                            ecgOutput.write(value + "\n");
                        else if (ltf != null)
                            ecgOutput.write(ltf.transformFloat(value) + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        timeAlign.getOutputData().clear();
    }

    /**
     * Callback that reports to the Android log.
     *
     * Implement this callback to provide functionality for PCard-like gadgets
     *
     * Implemented are those you need for typical usage
     */
    PcardGadgetConnectionCallback callback = new PcardGadgetConnectionCallback() {
        /**
         * This callback is used to notify the user that EcgData has been received
         */
        @Override
        public void onEcgDataReceived(UnpackedEcgData ecgData, byte[] rawData) {
            // increase sample counters
            numSamplesReceived += ecgData.values.length;
            numSamplesReceivedFromStart += ecgData.values.length;

            /**
             * The following code is not absolutely correct but shows what analysis can be done in
             * real time. To count the missing samples correctly, use time alignment output
             */
            // check if this is first sample after successful connection
            if (ecgData.discontinuedSampling) {
                lastPackageCounter = ecgData.sampleCounter;
                disconnectWhileSampling = true;
            } else if (ecgData.sampleCounterUnsure) {
                unsureCounter = true; //sample counter is not necessary precise
            } else{
                if(ecgData.sampleCounter < lastPackageCounter)
                    lastPackageCounter%=1024;
                samplesMissing += ecgData.sampleCounter - (lastPackageCounter + ecgData.values.length);
            }
            lastPackageCounter = ecgData.sampleCounter;

            // register the data with time alignment
            TsPair p = new TsPair(ecgData);
            boolean outputWasFlushed = timeAlign.registerPacket(p);
            // store the output as it is produced by the time alignment algorithm
            if (outputWasFlushed) {
                storeEcgData();
            }

            // when more than ECG_OUTPUT_REPORT_TIME has passed, report statistics
            long now = System.currentTimeMillis();
            if (now > (lastStreamingDataReportTime + ECG_OUTPUT_REPORT_TIME)) {
                LogEvent("onEcgDataReceived; \n  " +
                                (numSamplesReceived) + " samples received and "+
                                samplesMissing + " lost in the last "+
                                ((now-lastStreamingDataReportTime)*0.001)+" seconds, " +
                                numSamplesReceivedFromStart + " received altogether" +
                                (disconnectWhileSampling?"\nDisconnect occurred during this time!":"")+
                                (unsureCounter?"\nA long pause has occurred while sampling!":""));
                disconnectWhileSampling = false;
                unsureCounter = false;
                numSamplesReceived = 0;
                samplesMissing = 0;
                lastStreamingDataReportTime = now;
            }
        }

        @Override
        public void onErrorInDataMessage(String errorString) {
        }

        @Override
        public void onNewAsciiStringReceived(String s) {

        }

        /**
         * This callback will be issued if a warning is produced by the PWP library
         */
        @Override
        public void onWarning(String warning) {
            LogEvent("onWarning: "+ warning);
        }

        @Override
        public void onDebugMessage(String message) {
        }

        /**
         * This callback will be issued when a battery level change is detected
         * @param gadgetMeta the GadgetMeta instance, which contains all variables related to battery level
         */
        @Override
        public void onBatteryLevelChanged(GadgetMeta gadgetMeta) {
            LogEvent("onBatteryLevelChanged "+gadgetMeta.batteryObservable.get());
        }

        /**
         * Reports RSSI (wireless signal strength) value to the user
         * @param rssiLevel the RSSI level [-100..0]
         */
        @Override
        public void onRssiRead(int rssiLevel) {
            LogEvent("onRssiRead "+rssiLevel);
        }

        @Override
        public void onGadgetStateChanged(PcardGadgetBleCommunicator.GadgetState state) {
            LogEvent( "onGadgetStateChanged:\n" +
                    "  sf: " + state.getECGSamplingFreq()+"Hz\n"+
                    "  lowBatt: " + state.batteryLow()+
                    "  charging: " + state.isOnCharger()+"\n"+
                    "  supports acc: " + (state.accelerometerExists() ? "yes" : "no")
                    );
        }

        /**
         * This callback notifies the user that the either idle or sleep time have changed.
         * Idle time is counted when the gadget is connected but no data is being streamed.
         * Sleep time is counted when the PWP library is waiting before it attempts to connect to the gadget
         *
         * @param idleTime the time gadget has been idle (receiving gadgetData is way overdue) (<0 if not applicable)
         * @param sleepTime the time gadget will wait before attempting to reconnect (<0 if not applicable)
         */
        @Override
        public void onIdleTimeChanged(float idleTime, float sleepTime) {
            if (idleTime >= 0)
                LogEvent( "idle time changed: " + idleTime);
            else {
                // if idle time is invalid (< 0) and sleep time is 0, then the library is attempting to reconnect now  
                if (sleepTime == 0)
                    LogEvent( "reconnecting...");
                else
                    LogEvent( "sleeping for " + sleepTime + " seconds before reconnecting");
            }
        }

        @Override
        public void onConnectionStatusChange(BleCommunicator.FsmState state) {
        }

        /**
         * This will warn the user that incorrect pin has been supplied (this is checked upon connection to the gadget)
         * @param pinThatFailed value of the pin code that caused this
         */
        @Override
        public void onInvalidPin(String pinThatFailed) {
            LogEvent( "onInvalidPin\n\n INVALID PIN!\n");
        }

        @Override
        public void onConnectionParamsChanged(GadgetMeta gadgetMeta) {
            LogEvent( "onConnectionParamsChanged ");
        }

        /**
         * Notify the user that the measurement has been started and will be issued soon after thread.start()
         * is called if the procedure does not encounter any errors.
         */
        @Override
        public void onMeasurementStart() {
            LogEvent( "onMeasurementStart ");

            try {
                ecgOutput = new OutputStreamWriter(
                        new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+"/ecg.txt"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        /**
         * Notify the user that the measurement has ended and no mode data will be received.
         * At this point time alignment can be stopped and flushed and the remaining data stored to file.
         */
        @Override
        public void onMeasurementEnd() {
            LogEvent( "onMeasurementEnd ");

            try {
                timeAlign.stop();
                storeEcgData();
                if (ecgOutput != null) {
                    ecgOutput.close();
                    ecgOutput= null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            firmwareRevision = null;
        }

        @Override
        public void onGadgetConnected(boolean streamingStarted) {
            GadgetMeta gadgetMeta = thread.getMetaData();

            // setups sampling frequency of time alignment in [GHz] (one over nanoseconds)
            timeAlign.setupConstantFrequency(gadgetMeta.getSamplingFrequency() * 1e-9);
            if (firmwareRevision == null) {
                firmwareRevision = gadgetMeta.getFirmwareRevision();

                if (firmwareRevision != null) {
                    LogEvent("Connected to a gadget with firmware:\n " +
                            firmwareRevision.toString());
                }
            }

            LogEvent( "onGadgetConnected: "+
                    (streamingStarted ? "started streaming" : "idle"));

            if (streamingStarted) {
                if (gadgetMeta.getAdcTransformation() != null) {
                    ltf = gadgetMeta.getAdcTransformation();
                    logger.fine("Linear transformation is " + gadgetMeta.getAdcTransformation().toString());
                }
            }
        }

        /**
         * This callback is issued when the gadget disconnects from any reason.
         *
         * @param userIssued true if user issued the disconnect (an entity external to GadgetThread)
         * @param forced true if forced disconnect was used (might happen if BLE driver and BleCommunicator disagree on state of the connection)
         */
        @Override
        public void onGadgetDisconnected(boolean userIssued, boolean forced) {
            LogEvent( "onGadgetDisconnected ");
        }

    };

    /**
     * Class implementing scan callback from BleScanInterface
     * no need to use it in your project if you can provide MAC address on your own
     *
     * See function "scanForDevices" on how to initiate scan
     */
    private class ScanCallback implements BleScanInterface.DeviceScannedListener{
        @Override
        public void onScanStarted(boolean alreadyStarted) {
            LogEvent("scan callback: Scan started");
            MainActivity.this.enableItemSelection = false;
            runOnMainThread(new Runnable() {
                @Override
                public void run() {macSpinner.setEnabled(false);}});
        }

        @Override
        public boolean onScan(BluetoothDevice device, int rssi, byte[] scanRecord, long lastSeenInterval) {
            if(macSpinnerItems == null || macSpinnerItems.contains(device.getAddress()))
                return true;
            LogEvent("scan callback: Adding item to list " + device.getAddress());
            macSpinnerItems.add(device.getAddress());
            return true;
        }

        @Override
        public boolean onScanFinished(boolean timeout) {
            LogEvent("scan callback: Scan finished");
            runOnMainThread(new Runnable() {
                @Override
                public void run() {
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item);
                    for(String s: macSpinnerItems)
                        adapter.add(s);
                    macSpinner.setAdapter(adapter);
                    macSpinner.setEnabled(true);}
            });
            return false;
        }
    }

    /**
     * will start device scan using BleScanInterface
     */
    private void scanForDevices(){
        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                macSpinner.setAdapter(null);
                macSpinnerItems = new LinkedList<String>();
            }
        });
        
        BleScanInterface scanIn = BleScanInterface.getInstance(getApplicationContext());
        if(scanIn != null)
            scanIn.addScanListener(new ScanCallback(), null ,10000);
    }

    /**
     * Function that performs connect and registers the callback for receiving events and streaming data.
     * @param gadgetMeta instance of {@link GadgetMeta} that contains the required info for connection (address, pin, frequency, adc channel,
     *                   protocol, tx power)
     */
    private void connectTo(GadgetMeta gadgetMeta) {
        /* ADDITIONAL INFO
         *
         * Use WAKELOCK ( https://developer.android.com/training/scheduling/wakelock.html )
         * while thread is running
         * to ensure that BLE communication is working correctly
         * while application is in background
         *
         * Problems with lost or delayed transmissions were observed on older devices
         * with aggressive power management
         *
         * Main application should handle lifecycle of wakelock to minimize power consumption.
         */

        thread = new GadgetThread(this, gadgetMeta);
        thread.addCallback(callback);
        thread.start();
        // to change the period of periodic status checks (RSSI, battery level, gadget state), issue the command below
        //thread.setPeriodForStatusChecks(1000, 500);

        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                start.setEnabled(false);
                end.setEnabled(true);
            }
        });
    }

    /**
     * Function that performs disconnect.
     */
    private void disconnect() {
        if (thread != null) {
            thread.end();
        }
        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                start.setEnabled(true);
                end.setEnabled(false);
            }
        });
    }

    //region activity functionality

    /* Button listener callback */
    @Override
    public void onClick(View v) {
        //region start button clicked
        if (v.getId() == si.mobecgblelibrarytest.R.id.gadget_start_button) {
            String address = this.macAddressTV.getText().toString();
            String pin = this.pinTV.getText().toString();

            // save MAC and pin
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = sharedPref.edit();
            edit.putString("PIN", pin);
            edit.putString("MAC", address);
            edit.apply();

            // connect to gadget
            int freq = 125;
            try{
                freq = Math.round(Float.parseFloat(this.frequency.getText().toString()));
            }catch (Exception e){
                Toast.makeText(getBaseContext(), "WRONG FREQUENCY (using 125)!", Toast.LENGTH_SHORT).show();
            }
            byte adc = (byte)this.adcSelect.getProgress();
            byte mode = (byte)this.modeSelect.getProgress();
            byte txPower = (byte)this.txPowerSelect.getProgress();
            byte adcSelect = PcardGadgetBleCommunicator.Command.ECG_RANGE_ANALOG_AMP_6mV;

            final GadgetMeta gadgetMeta = new GadgetMeta(address, "Name",
                    -75, 255, 0, "version", pin, freq, adc, mode, txPower, adcSelect);
            connectTo(gadgetMeta);
        }
        //end button clicked
        if (v.getId() == si.mobecgblelibrarytest.R.id.gadget_end_button) {
            disconnect();
        }
        //endregion
        //region scan button clicked
        if(v.getId() == si.mobecgblelibrarytest.R.id.scan_button){
            scanForDevices();
        }
        //endregion
        //region clear buffer button clicked
        if(v.getId() == si.mobecgblelibrarytest.R.id.clear_button){
            runOnMainThread(new Runnable() {
                @Override
                public void run() {
                    debugOutput.setText("");
                }
            });
        }
        //endregion
        //region help button clicked
        if(v.getId() == si.mobecgblelibrarytest.R.id.help_button){
            runOnMainThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), "" +
                            "Press 'SCAN' to get MACs of BLE devices around\n\n" +
                            "Select one and enter its PIN, then click 'START'\n\n" +
                            "Device should connect and its status should be shown on screen.\n" +
                            "To disconnect from device click 'END'.\n" +
                            "to clear status messages click 'CLEAR BUFFER'" +
                            "", Toast.LENGTH_LONG).show();
                }
            });
        }
        //endregion
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(si.mobecgblelibrarytest.R.layout.activity_main);
        initializeView();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        /* ADDITIONAL INFO
         * Use this function to turn off (or reduce) output on ADB by library
         *
         * Debug output of the library is useful additional information to module developers
         * in case of any problems.
         */
        setupLogger();
    }

    @Override
    protected void onDestroy() {
        disconnect();
        super.onDestroy();
    }

    /* set up GUI elements */
    private void initializeView() {
        this.handler = new Handler(Looper.getMainLooper());

        this.macAddressTV = (TextView) findViewById(si.mobecgblelibrarytest.R.id.gadget_mac_address);
        this.pinTV = (TextView) findViewById(si.mobecgblelibrarytest.R.id.gadget_pin);
        this.frequency = (TextView) findViewById(si.mobecgblelibrarytest.R.id.frequency);
        this.start = (Button) findViewById(si.mobecgblelibrarytest.R.id.gadget_start_button);
        this.start.setOnClickListener(this);
        this.end = (Button) findViewById(si.mobecgblelibrarytest.R.id.gadget_end_button);
        this.end.setEnabled(false);
        this.end.setOnClickListener(this);
        this.adcSelect = (SeekBar)findViewById(si.mobecgblelibrarytest.R.id.adcSeek);
        this.adcSelect.setOnSeekBarChangeListener(this);
        this.modeSelect = (SeekBar)findViewById(si.mobecgblelibrarytest.R.id.modeSeek);
        this.modeSelect.setOnSeekBarChangeListener(this);
        this.txPowerSelect = (SeekBar)findViewById(si.mobecgblelibrarytest.R.id.txPowerSeek);
        this.txPowerSelect.setOnSeekBarChangeListener(this);

        this.macSpinner = (Spinner) findViewById(si.mobecgblelibrarytest.R.id.mac_spinner);
        this.macSpinner.setOnItemSelectedListener(this);
        this.scanButton = (Button) findViewById(si.mobecgblelibrarytest.R.id.scan_button);
        this.scanButton.setOnClickListener(this);

        this.debugOutput = (TextView) findViewById(si.mobecgblelibrarytest.R.id.debug_text);
        this.debugScroll = (ScrollView) findViewById(si.mobecgblelibrarytest.R.id.debug_scroll);
        this.clearDebug = (Button) findViewById(si.mobecgblelibrarytest.R.id.clear_button);
        this.clearDebug.setOnClickListener(this);
        this.helpButton = (Button) findViewById(si.mobecgblelibrarytest.R.id.help_button);
        this.helpButton.setOnClickListener(this);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String pin = sharedPref.getString("PIN", this.pinTV.getText().toString());
        String mac = sharedPref.getString("MAC", this.macAddressTV.getText().toString());
        this.pinTV.setText(pin);
        this.macAddressTV.setText(mac);

        // Check whether this app has write external storage permission or not.
        int writeExternalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
// If do not grant write external storage permission.
        if(writeExternalStoragePermission!= PackageManager.PERMISSION_GRANTED)
        {
            // Request user to grant write external storage permission.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }


    //region seekbar callbacks
    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {}

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}

    //endregion seekbar callbacks

    //region spinner callbacks
    private boolean enableItemSelection = false;
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(!enableItemSelection){
            enableItemSelection = true;
            if(macAddressTV.getText().length()>0) //if MAC is written in field do not update
                return;
        }
        final TextView v = (TextView) view;
        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                if(v==null)
                    return;
                macAddressTV.setText(v.getText());
                pinTV.setText("");
            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
    //endregion spinner callbacks

    //region helpers
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        LogEvent("onRequestPermissionsResult called");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    void LogEvent(final String msg){
        logger.config(msg);
        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                debugOutput.append("MainActivity:"+msg+"\n");
                debugScroll.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    void runOnMainThread(Runnable rn){
        if (handler != null)
            handler.post(rn);
    }
    //endregion helpers

    //endregion activity functionality

    private void setupLogger() {
        logger.setLevel(Level.FINE);

        // ###########################################################################################################
        // setup logging
        AndroidLoggingHandler.init();
    }
}
